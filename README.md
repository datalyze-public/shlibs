# shlibs

Useful shell functions for shared usage, e.g. in docker entrypoints or for CI jobs.

## Install

```bash
cd /opt/
sudo git clone https://github.com/datalyze-public/shlibs
sudo ln -s /opt/shlibs /usr/local/bin
```

## Usage

### Bash

Simply import `index.sh` to import all shlibs functions.

```bash
source /usr/local/bin/shlibs/index.sh
```

### Options

You can predefine several options via environment variables or during import.

| Parameter                            | Description                                                                |
| ------------------------------------ | -------------------------------------------------------------------------- |
| -d, --debug                          | Enable debugging, sets SHLIBS_DEBUG=true.                                  |
| -r, --reload                         | Reload the shlibs functions. Useful on development.                        |
| -e, --export, --export-for-subshells | Exports all shlibs functions (e.g. export -f shlibs.some-function).        |
|                                      | Enables usage of shlibs inside subshells, e.g. in CI or task runner tools. |
| -h, --help                           | Shows this usage instructions.                                             |

#### Examples

```bash
source <(/usr/local/bn/shlibs/index.sh --reload --export)
shlibs.log_info "Hello World!"
bash -c "shlibs.log_info "Hello World from this subshell!!!"
```

#### Logging Levels

Shlibs provides logging levels with ordinal skale. Each level has a priority.
* debug (low prio) > info > warning > error (high prio)

Valid input:
* DEBUG, debug
* INFO, info
* WARN, WARNING, warn, warning
* ERR, ERROR, err, error

```bash
# log messages with debug level and higher priority
SHLIBS_LOGGING_LEVEL="debug"

# log messages with warning level and higher: only warning and error
SHLIBS_LOGGING_LEVEL="warning"
```

#### Logging mode

Shlibs provides two modes: `echo` and `logger. Echo uses the `echo` command and logger the `logger` shell command. Default is `logger`. On some platforms echo might not exist so you need to check or test to find the best working mode, e.g. inside a ubuntu:22.04 container, logger does not work.

```bash
export SHLIBS_LOGGING_MODE="echo"
```

### Logging stats

Shlibs can log additional information like time, user, loglevel and the function the log command was used in.

```bash
source /usr/local/bin/shlibs/index.sh --reload
shlibs.log_info "HALLO"
# results in the following message
priority="user.info" time="2023-09-13 10:41:06.3492" level="user.info" app="bash" msg="HALLO"

LOGGING_STATS="false"
source /usr/local/bin/shlibs/index.sh --reload
shlibs.log_info "HALLO"
# results in the following message (depending on SHLIBS_LOGGING_MODE)
HALLO
```

### Inside Container

```Dockerfile
FROM registry.gitlab.com/datalyze-public/shlibs:latest AS shlibs

FROM ubuntu:22.04
COPY --from=shlibs /usr/local/bin/shlibs /usr/local/bin/shlibs
```

### Using in subshell

You can export all shlibs functions to be usable in a subshell. This is useful for CI environments or task tools (e.g. just, make, etc.) which spin up a new shell for each task. Export is disabled by default.

```bash
source /usr/local/bin/shlibs/index.sh
shlibs.export_functions_to_subshell
bash -c "shlibs.log_info HALLO"
```

or

```bash
source /usr/local/bin/shlibs/index.sh --export
bash -c "shlibs.log_info HALLO"
```

## Development

If you change shlibs functions, you need to import index.sh with the `--reload` flag. This will deregister the functions before resourcing them.

```bash
source /usr/local/bin/shlibs/index.sh --reload
```

### Useful tools



* [hadolint](https://github.com/hadolint/hadolint)
* [shellcheck](https://github.com/koalaman/shellcheck): `sudo apt install shellcheck`
* [yamllint](https://github.com/adrienverge/yamllint): `sudo apt install yamllint` or `pip install --user yamllint`
* [commitlint](https://commitlint.js.org/guides/getting-started.html): `npm install --save-dev @commitlint/{cli,config-conventional}`
* [conventional-pre-commit](https://github.com/compilerla/conventional-pre-commit)

### Githooks

This repo uses [pre-commit](https://pre-commit.com/) to improve code quality. You must install and init the framework accordingly.

```bash
sudo apt install pre-commit
# or
pip install --user pre-commit

# init pre-commit
pre-commit install
```