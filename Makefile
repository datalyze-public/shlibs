.EXPORT_ALL_VARIABLES:
SHELL := /bin/bash

LOCAL_COMMIT_SHORT_SHA = $(shell git rev-parse --short HEAD)
LOCAL_COMMIT_REF_SLUG = $(shell git rev-parse --abbrev-ref HEAD)
COMMIT_SHORT_SHA ?= $(LOCAL_COMMIT_SHORT_SHA)
COMMIT_BRANCH ?= $(LOCAL_COMMIT_REF_SLUG)
# BUILD_VERSION ?= $(shell git describe --tags $(shell git rev-list --tags --max-count=1) || echo "0.1.0")
BUILD_VERSION?=$(shell commit-and-tag-version --dry-run | grep "tagging release" | cut -d " " -f 4)
BUILD_VERSION_MAJOR?=$(shell commit-and-tag-version --dry-run | grep "tagging release" | cut -d " " -f 4 | cut -d "." -f 1)
BUILD_DATE?=$(shell date -u +'%Y-%m-%dT%H:%M:%SZ')

IMAGE_NAME?=registry.gitlab.com/datalyze-public/shlibs
IMAGE_DEV_TAG?=${BUILD_VERSION}-${COMMIT_SHORT_SHA}-dirty
IMAGE_RELEASE_TAG_BRANCH_VERSION?=${BUILD_VERSION}-main
IMAGE_DEV_GUN?=${IMAGE_NAME}:${IMAGE_DEV_TAG}
IMAGE_RELEASE_GUN?=${IMAGE_NAME}:${IMAGE_RELEASE_TAG}

DESCRIPTION?=Useful shell functions for shared usage, e.g. in docker entrypoints or for CI jobs.
MAINTAINER?=Matthias Ludwig <m.ludwig@datalyze-solutions.com>
URL?=https://gitlab.com/datalyze-public/shlibs
VSC_URL?=https://gitlab.com/datalyze-public/shlibs
VENDOR=Datalyze Solutions GmbH

version:
	@echo "COMMIT_BRANCH: $(COMMIT_BRANCH)"
	@echo "COMMIT_SHORT_SHA: $(COMMIT_SHORT_SHA)"
	@echo "BUILD_VERSION: $(BUILD_VERSION)"
	@echo "BUILD_VERSION_MAJOR: ${BUILD_VERSION_MAJOR}"
	@echo "BUILD_DATE: ${BUILD_DATE}"
	@echo "IMAGE_DEV_GUN:  ${IMAGE_DEV_GUN}"
	@echo "IMAGE_RELEASE_GUN:  ${IMAGE_RELEASE_GUN}"

skaffold-disable-telemetry:
	skaffold config set --global collect-metrics false

push: skaffold-disable-telemetry
	skaffold push ${IMAGE_DEV_GUN}

build: skaffold-disable-telemetry
	skaffold build --tag "${IMAGE_DEV_TAG}"

tag-with-%: IMAGE_RELEASE_TAG=latest
tag-with-%:
	crane copy ${IMAGE_DEV_GUN} ${IMAGE_RELEASE_GUN}
tag-with-version: IMAGE_RELEASE_TAG=${BUILD_VERSION}
tag-with-version:
tag-with-branch-version: IMAGE_RELEASE_TAG=${IMAGE_RELEASE_TAG_BRANCH_VERSION}
tag-with-branch-version:
tag-with-branch-name: IMAGE_RELEASE_TAG=${COMMIT_BRANCH}
tag-with-branch-name:
tag-with-latest:
tag-with-major: IMAGE_RELEASE_TAG=${BUILD_VERSION_MAJOR}
tag-with-major:

gitlab-release: version tag-with-branch-version tag-with-branch-name tag-with-major tag-with-latest sync-to-dockerhub
# the full gitlab pipeline
gitlab-all: build gitlab-release ci-multiarch-build sync-to-dockerhub

SHLIBS_TEST_IMAGE_GUN=shlibs-test
test-build:
	docker build -t "${SHLIBS_TEST_IMAGE_GUN}" test/
test-run:
	docker run -it --rm "${SHLIBS_TEST_IMAGE_GUN}" bash

compile-to-yaml:
	./compile-to-yaml.sh

hooks-install:
	pip install --user pre-commit
# mkver is outdated and not well maintained
# mkver-install:
# 	mkdir /tmp/mkver
# 	cd /tmp/mkver
# 	wget -O /tmp/mkver/git-mkver.tar.gz https://github.com/idc101/git-mkver/releases/download/v1.3.0/git-mkver-linux-x86_64-1.3.0.tar.gz
# 	tar -xvf /tmp/mkver/git-mkver.tar.gz
# 	sudo install git-mkver

commit-and-tag-version-install:
	npm install --global commit-and-tag-version
release-dry:
	commit-and-tag-version --dry-run
release:
	commit-and-tag-version

CI_IMAGE_GUN?=registry.gitlab.com/datalyze-public/shlibs/ci-tools:main
ci-build:
	docker build --pull -t ${CI_IMAGE_GUN} docker/ci-tools
ci-push:
	docker push ${CI_IMAGE_GUN}
ci-multiarch-build: skaffold-disable-telemetry
	cd docker/ci-tools && \
	skaffold build --tag "${CI_IMAGE_GUN}"

hadolint-shlibs:
	hadolint docker/shlibs/Dockerfile
hadolint-test:
	hadolint docker/test/Dockerfile
hadolint-ci:
	hadolint docker/ci/Dockerfile

yamllint-shlibs:
	yamllint yaml/shlibs.yaml

shellcheck-shlibs:
	shellcheck shlibs/*.sh
	shellcheck shlibs/bin/*.sh

git-hook-run:
	pre-commit run

sync-to-dockerhub:
	crane copy registry.gitlab.com/datalyze-public/shlibs:latest datalyze/shlibs:latest
	crane copy registry.gitlab.com/datalyze-public/shlibs:latest datalyze/shlibs:${BUILD_VERSION}
	crane copy registry.gitlab.com/datalyze-public/shlibs:latest datalyze/shlibs:${COMMIT_BRANCH}
	crane copy registry.gitlab.com/datalyze-public/shlibs:latest datalyze/shlibs:${IMAGE_RELEASE_TAG_BRANCH_VERSION}
	crane copy registry.gitlab.com/datalyze-public/shlibs:latest datalyze/shlibs:${BUILD_VERSION_MAJOR}