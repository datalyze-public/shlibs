set dotenv-load
set export

_COMMA := ","
_COLON := ":"
_DOT := "."
_SEMICOLON := ";"
_MINUS := "-"
_SPACE := " "
_SLASH := "/"
_BACKSLASH := '\'
_UNDERSCORE := "_"

PROJECT_NAME := "shlibs"

_TAG_DIRTY := "dirty"
_TAG_DEFAULT_BRANCH := env_var_or_default("CI_DEFAULT_BRANCH", "main")

TARGETOS_LINUX := "linux"
TARGETPLATFORM_AMD64 := "amd64"
TARGETPLATFORM_ARM64 := "arm64"
PLATFORM_AMD64 := TARGETOS_LINUX + _SLASH + TARGETPLATFORM_AMD64
PLATFORM_ARM64 := TARGETOS_LINUX + _SLASH + TARGETPLATFORM_ARM64

RELEASE_CLI := "commit-and-tag-version"

# explicitly add -dirty to dev tag, cause --dirty flag sometimes failes
# take care to use a default 'v1.0.0' if no tag is set at all
BUILD_VERSION_DEV := `echo "$(git describe --always --tags --long)-dirty"`
BUILD_VERSION_RELEASE := `git describe --always --tags --long`
BUILD_VERSION := `git describe --tags | cut -d '-' -f 1`
BUILD_VERSION_MAJOR := `git describe --tags | cut -d '-' -f 1 | cut -d "." -f 1`
BUILD_VERSION_MINOR := BUILD_VERSION_MAJOR + _DOT + `git describe --tags | cut -d '-' -f 1 | cut -d "." -f 2`
COMMITS_SINCE_LAST_TAG := `git describe --tags | cut -d '-' -f 2`
BUILD_DATE := `date -u +'%Y-%m-%dT%H:%M:%SZ'`

CI_COMMIT_REF_SLUG := env_var_or_default("CI_COMMIT_REF_SLUG", `git rev-parse --abbrev-ref HEAD`)
CI_COMMIT_SHORT_SHA := env_var_or_default("CI_COMMIT_SHORT_SHA", `git rev-parse HEAD | head -c 8`)
CI_REGISTRY_IMAGE := env_var_or_default("CI_REGISTRY_IMAGE", "registry.gitlab.com/datalyze-public/shlibs")
IMAGE_NAME := CI_REGISTRY_IMAGE
IMAGE_DEV_TAG := BUILD_VERSION_DEV
IMAGE_RELEASE_TAG_BRANCH_VERSION := BUILD_VERSION + _MINUS + CI_COMMIT_REF_SLUG
IMAGE_RELEASE_TAG_BRANCH_VERSION_FULL := BUILD_VERSION_RELEASE + _MINUS + CI_COMMIT_REF_SLUG
IMAGE_RELEASE_TAG := BUILD_VERSION_RELEASE

IMAGE_DEV_GUN := IMAGE_NAME + _COLON + IMAGE_DEV_TAG
IMAGE_RELEASE_GUN := IMAGE_NAME + _COLON + BUILD_VERSION_RELEASE

@recipes:
    just --list --justfile {{justfile()}}

@version:
    printf "%-23s %s\n" "BUILD_VERSION:" {{BUILD_VERSION}}
    printf "%-23s %s\n" "BUILD_VERSION_MAJOR:" {{BUILD_VERSION_MAJOR}}
    printf "%-23s %s\n" "BUILD_DATE:" {{BUILD_DATE}}
    printf "%-23s %s\n" "CI_COMMIT_REF_SLUG:" {{CI_COMMIT_REF_SLUG}}
    printf "%-23s %s\n" "CI_COMMIT_SHORT_SHA:" {{CI_COMMIT_SHORT_SHA}}
    echo
    printf "%-23s %s\n" "IMAGE_DEV_TAG:" {{IMAGE_DEV_TAG}}
    printf "%-23s %s\n" "IMAGE_RELEASE_TAG:" {{IMAGE_RELEASE_TAG}}
    echo
    printf "%-23s %s\n" "IMAGE_DEV_GUN:" {{IMAGE_DEV_GUN}}
    printf "%-23s %s\n" "IMAGE_RELEASE_GUNs:" {{IMAGE_RELEASE_GUN}}
    printf "%-23s %s:%s\n" "" {{IMAGE_NAME}} {{CI_COMMIT_REF_SLUG}}
    printf "%-23s %s:%s\n" "" {{IMAGE_NAME}} {{IMAGE_RELEASE_TAG_BRANCH_VERSION}}
    printf "%-23s %s:%s\n" "" {{IMAGE_NAME}} {{IMAGE_RELEASE_TAG_BRANCH_VERSION_FULL}}
    printf "%-23s %s:%s\n" "" {{IMAGE_NAME}} {{BUILD_VERSION_MINOR}}
    printf "%-23s %s:%s\n" "" {{IMAGE_NAME}} {{BUILD_VERSION_MAJOR}}
    echo
    echo "Tag GUNs"
    printf "%-23s %s:%s\n" "" {{IMAGE_NAME}} {{BUILD_VERSION}}

release-dry:
    {{RELEASE_CLI}} --dry-run
release:
    {{RELEASE_CLI}}
release-init:
    {{RELEASE_CLI}} --first-release

@_tag-with-subimages IMAGE_RELEASE_TAG="latest" SUB_IMAGE_SUFFIXES="ci-tools":
    #!/usr/bin/env bash
    set -euxo pipefail
    sub_image_suffixes=({{SUB_IMAGE_SUFFIXES}})
    for sub_image_suffix in "${sub_image_suffixes[@]}"; do
        LOCAL_DEV_GUN={{IMAGE_NAME + _SLASH}}${sub_image_suffix}{{_COLON + IMAGE_DEV_TAG}}
        LOCAL_RELEASE_GUN={{IMAGE_NAME + _SLASH}}${sub_image_suffix}{{_COLON + IMAGE_RELEASE_TAG}}
        crane copy ${LOCAL_DEV_GUN} ${LOCAL_RELEASE_GUN}
    done
@_tag-with IMAGE_RELEASE_TAG="latest":
    #!/usr/bin/env bash
    LOCAL_DEV_GUN={{IMAGE_NAME + _COLON + IMAGE_DEV_TAG}}
    LOCAL_RELEASE_GUN={{IMAGE_NAME + _COLON + IMAGE_RELEASE_TAG}}
    crane copy ${LOCAL_DEV_GUN} ${LOCAL_RELEASE_GUN}
tag-with-branch: (_tag-with CI_COMMIT_REF_SLUG)
tag-with-branch-version: (_tag-with IMAGE_RELEASE_TAG_BRANCH_VERSION)
tag-with-branch-version-full: (_tag-with IMAGE_RELEASE_TAG_BRANCH_VERSION_FULL)
tag-with-latest: (_tag-with)
tag-with-minor: (_tag-with BUILD_VERSION_MINOR)
tag-with-major: (_tag-with BUILD_VERSION_MAJOR)
tag-with-version: (_tag-with BUILD_VERSION)

build *ARGS="":
    skaffold build {{ARGS}}
build-push: (build "--push")

compile-to-yaml:
	./compile-to-yaml.sh

hadolint-shlibs:
	hadolint docker/shlibs/Dockerfile
hadolint-test:
	hadolint docker/test/Dockerfile

yamllint-shlibs:
	yamllint yaml/shlibs.yaml

shellcheck-shlibs:
	shellcheck shlibs/*.sh
	shellcheck shlibs/bin/*.sh

git-hook-run:
	pre-commit run

sync-to-dockerhub:
	crane copy registry.gitlab.com/datalyze-public/shlibs:latest datalyze/shlibs:latest
	crane copy registry.gitlab.com/datalyze-public/shlibs:latest datalyze/shlibs:${BUILD_VERSION}
	crane copy registry.gitlab.com/datalyze-public/shlibs:latest datalyze/shlibs:${COMMIT_BRANCH}
	crane copy registry.gitlab.com/datalyze-public/shlibs:latest datalyze/shlibs:${IMAGE_RELEASE_TAG_BRANCH_VERSION}
	crane copy registry.gitlab.com/datalyze-public/shlibs:latest datalyze/shlibs:${BUILD_VERSION_MAJOR}
