#!/usr/bin/env bash

source ./shlibs/index.sh

# tempdir=$(mktemp --directory)
tempdir="/tmp/shlibs"
mkdir -p "${tempdir}"

# join all shlibs files and indent them accordingly to be used in the resulting gitlab yaml
sed 's_^_      _' ./shlibs/bin/*.sh | tee "${tempdir}/shlibs-indented" > /dev/null
cat ./yaml/shlibs.yaml.template "${tempdir}/shlibs-indented" | tee "${tempdir}/shlibs.yaml" > /dev/null
# add linebreak to end of file
#echo "" | tee -a "${tempdir}/shlibs.yaml"
# remove shebangs
sed -i 's/\#\!\/usr\/bin\/env bash//g' "${tempdir}/shlibs.yaml"
# trim trailing whitespaces
sed -i 's/[ \t]*$//' "${tempdir}/shlibs.yaml"

yamllint "${tempdir}/shlibs.yaml"
lintresult="$?"
if [ ${lintresult} -eq 0 ]; then
    cp "${tempdir}/shlibs.yaml" "./yaml/shlibs.yaml"
fi
shlibs.log_info "Compiled shlibs to: ./yaml/shlibs.yaml"