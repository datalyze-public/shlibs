#!/usr/bin/env bash

shlibs.generic.login.usage() {
    local tool="${FUNCNAME[-1]}"

    cat <<EOF
${tool} parameters

Login to a remote registry.

This functions should not be used directly. Better use the tool specific versions:

* acorn.login
* buildah.login
* crane.login
* docker.login
* podman.login

Parameters
-t, --tool        Tool command used for login, defaults to 'docker login'.
-r, --registry    Registry to login.
-u, --username    Username used to login to registry.
-p, --password    Password used to login to registry.
-h, --help        Shows this usage instructions.

Example
$ shlibs.generic.login --tool docker --registry registry.gitlab.com --username my-gitlab-username --password my-super-secret-password
$ acorn.login --registry registry.gitlab.com --username my-gitlab-username --password my-super-secret-password

EOF
}

shlibs.generic.login() {
    local VALID_ARGS=$(getopt -o t:r:u:p: --long help,tool:,registry:,username:,password:,password-stdin -- "$@")
    local VALID_ARGS_COUNT=0
    if [[ $? -ne 0 ]]; then
        return 1
    fi

    local PASSWORD_STDIN=false

    eval set -- "$VALID_ARGS"
    while [ : ]; do
        case "$1" in
        -r | --registry)
            local REGISTRY="${2:-${CI_REGISTRY}}"
            shift 2
            VALID_ARGS_COUNT=$((VALID_ARGS_COUNT + 1))
            ;;
        -u | --username)
            local USERNAME="${2:-gitlab-ci-token}"
            shift 2
            VALID_ARGS_COUNT=$((VALID_ARGS_COUNT + 1))
            ;;
        -p | --password)
            local PASSWORD="${2:-${CI_JOB_TOKEN}}"
            shift 2
            VALID_ARGS_COUNT=$((VALID_ARGS_COUNT + 1))
            ;;
        --password-stdin)
            PASSWORD_STDIN=true
            shift
            ;;
        -t | --tool)
            local TOOL_CMD="${2:-docker login}"
            shift 2
            VALID_ARGS_COUNT=$((VALID_ARGS_COUNT + 1))
            ;;
        -h | --help)
            shlibs.generic.login.usage
            break
            return
            ;;
        --)
            shift
            break
            ;;
        esac
    done

    if ((${VALID_ARGS_COUNT} <= 1)); then
        shlibs.generic.login.usage
        return 1
    fi

    if [ -z "${USERNAME}" ]; then
        shlibs.log_error "No '--username' defined"
        return 2
    fi

    if [ -z "${REGISTRY}" ]; then
        shlibs.log_error "No '--registry' defined"
        return 3
    fi

    if [ -z "${PASSWORD}" ]; then
        shlibs.log_error "No 'password' defined"
        read -s -p "PASSWORD: " -e PASSWORD
    fi

    if $PASSWORD_STDIN; then
        read -s -p "PASSWORD: " -e PASSWORD
    fi

    shlibs.log_debug "Tool: ${TOOL_CMD}, Registry: ${REGISTRY}, Username: ${USERNAME}"
    echo "${PASSWORD}" | ${TOOL_CMD} login --username "${USERNAME}" --password-stdin "${REGISTRY}"
}

shlibs.acorn.login() {
    shlibs.generic.login --tool "acorn" "$@"
}

shlibs.crane.login() {
    shlibs.generic.login --tool "crane auth" "$@"
}

shlibs.docker.login() {
    shlibs.generic.login --tool "docker" "$@"
}

shlibs.buildah.login() {
    shlibs.generic.login --tool "buildah" "$@"
}

shlibs.podman.login() {
    shlibs.generic.login --tool "podman" "$@"
}

shlibs.acorn.extract_image_from_acorn.usage() {
    cat <<EOF
extract-image-from-acorn: [parameters] <source-gun> <target-gun>

You need to be login to the source and target registries seperatly!

Parameters
-t|--tool     tool used for extraction, can be one of: [acorn, crane], defaults to: acorn
-n|--name     name of the image to extract
-d|--debug    enables debugging (simply sets DEBUG=true)
-h|--help     shows this usage instructions

Example
$ # copy the 'alpine' named image from the my-acorn-app:main
$ ./extract-image-from-acorn -t acorn -n alpine registry.gitlab.com/my-company/my-acorn-app:main registry.gitlab.com/my-company/my-acorn-app/alpine:main

EOF
}

shlibs._copy_image() {
    local SOURCE_GUN_SUBIMAGE="${1}"
    local TARGET_GUN="${2}"
    local EXTRACTION_TOOL="${3:-crane}"

    if [ -z ${SOURCE_GUN+x} ]; then
        shlibs.log_error "SOURCE_GUN is unset"
        return 1
    fi
    if [ -z ${TARGET_GUN+x} ]; then
        shlibs.log_error "TARGET_GUN is unset"
        return 1
    fi

    case "${EXTRACTION_TOOL}" in
    "acorn")
        shlibs.log_debug "acorn copy --force '${SOURCE_GUN_SUBIMAGE}' '${TARGET_GUN}'"
        acorn copy --force ${SOURCE_GUN_SUBIMAGE} ${TARGET_GUN}
        ;;
    "crane")
        shlibs.log_debug "crane copy '${SOURCE_GUN_SUBIMAGE}' '${TARGET_GUN}'"
        crane copy ${SOURCE_GUN_SUBIMAGE} ${TARGET_GUN}
        ;;
    esac
}

shlibs._copy_image_acorn() {
    local SOURCE_GUN_SUBIMAGE="${1}"
    local TARGET_GUN="${2}"
    shlibs._copy_image "${SOURCE_GUN_SUBIMAGE}" "${TARGET_GUN}" "acorn"
}

shlibs._copy_image_crane() {
    local SOURCE_GUN_SUBIMAGE="${1}"
    local TARGET_GUN="${2}"
    shlibs._copy_image "${SOURCE_GUN_SUBIMAGE}" "${TARGET_GUN}" "crane"
}

shlibs.acorn._extract_image_from_acorn() {
    local SOURCE_GUN="${1}"
    local TARGET_GUN="${2}"
    local IMAGE_NAME_TO_EXTRACT="${3:-all}"
    local EXTRACTION_TOOL="${4:-crane}"

    shlibs.log_debug "Will search for image named: ${IMAGE_NAME_TO_EXTRACT}"

    MANIFEST_DIGESTS=($(crane manifest "${SOURCE_GUN}" | jq -r .manifests[].digest))
    shlibs.log_debug "The manifest containts the following MANIFEST_DIGESTS: ${MANIFEST_DIGESTS[@]}"

    for IMAGE_DIGEST in "${MANIFEST_DIGESTS[@]}"; do
        SOURCE_GUN_SUBIMAGE="${SOURCE_GUN}@${IMAGE_DIGEST}"
        IMAGE_NAME=$(crane config "${SOURCE_GUN_SUBIMAGE}" | jq -r '.config.Labels."org.label-schema.name"')
        shlibs.log_debug "Manifest contains image named: ${IMAGE_NAME} with DIGEST: ${IMAGE_DIGEST}"

        # null means we found the acorn app
        case "${IMAGE_NAME_TO_EXTRACT}" in
        all | ALL)
            if [ "${IMAGE_NAME}" != "null" ]; then
                shlibs._copy_image "${SOURCE_GUN_SUBIMAGE}" "${TARGET_GUN}-${IMAGE_NAME}" "${EXTRACTION_TOOL}"
            fi
            ;;
        *)
            if [ "${IMAGE_NAME}" != "null" ] && [ "${IMAGE_NAME}" = "${IMAGE_NAME_TO_EXTRACT}" ]; then
                shlibs.log_debug "Found image named: ${IMAGE_NAME} with GUN: ${SOURCE_GUN_SUBIMAGE}"
                shlibs._copy_image "${SOURCE_GUN_SUBIMAGE}" "${TARGET_GUN}" "${EXTRACTION_TOOL}"
                return 0
            fi
            ;;
        esac
    done
}

shlibs.acorn.extract_image_from_acorn() {
    # default values
    local EXTRACTION_TOOL="acorn"
    local IMAGE_NAME_TO_EXTRACT="all"
    local SHLIBS_LOGGING_LEVEL="${SHLIBS_LOGGING_LEVEL:-info}"

    shlibs.log_debug "$@"

    VALID_ARGS=$(getopt -o t:n:dh --long tool:,name:,debug,help -- "$@")
    if [[ $? -ne 0 ]]; then
        # exit 1
        return 1
    fi

    eval set -- "$VALID_ARGS"
    while [ : ]; do
        case "$1" in
        -n | --name)
            IMAGE_NAME_TO_EXTRACT="$2"
            shift 2
            ;;
        -t | --tool)
            case "${2}" in
            acorn | crane)
                EXTRACTION_TOOL="${2}"
                ;;
            *)
                shlibs.log_error "Wrong parameter -t|--tool ${2}; has to be one of: [acorn, crane]"
                shlibs.acorn.extract_image_from_acorn.usage
                return 1
                ;;
            esac
            shift 2
            ;;
        -d | --debug)
            DEBUG=true
            SHLIBS_LOGGING_LEVEL="debug"
            shift
            ;;
        -h | --help)
            shlibs.acorn.extract_image_from_acorn.usage
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            shlibs.log_error "Unknown parameter: '$1'"
            shlibs.acorn.extract_image_from_acorn.usage
            break
            ;;
        esac
    done

    if [ -z ${1+x} ]; then
        shlibs.log_error "SOURCE_GUN is unset"
        shlibs.acorn.extract_image_from_acorn.usage
        return 1
    fi
    if [ -z ${2+x} ]; then
        shlibs.log_error "TARGET_GUN is unset"
        shlibs.acorn.extract_image_from_acorn.usage
        return 1
    fi
    SOURCE_GUN="${1}"
    shift
    TARGET_GUN="${1}"
    shift

    shlibs.log_debug "Tool: ${EXTRACTION_TOOL}"
    shlibs.log_debug "Images to extract: ${IMAGE_NAME_TO_EXTRACT}"
    shlibs.log_debug "SOURCE: ${SOURCE_GUN} TARGET: ${TARGET_GUN}"
    shlibs.acorn._extract_image_from_acorn "${SOURCE_GUN}" "${TARGET_GUN}" "${IMAGE_NAME_TO_EXTRACT}" "${EXTRACTION_TOOL}"
}

shlibs.acorn.digest_per_image.usage() {
    cat <<EOF
shlibs.acorn.digest_per_image parameters

Prints all sub image guns contained in an acorn app and writes an env file if -o|--output is set.

!!!Take care to login crane before using this function!!!
crane.login --registry registry.gitlab.com --username my-gitlab-username --password my-super-secret-password

Env file format:
    export GUN_<IMAGE_NAME>=<SOURCE_IMAGE_GUN>@<IMAGE_NAME_DIGEST>

Parameters
-o, --output      Path of the env file to export to digests to.
-e, --with-export Add 'export' before each env line.
-d, --debug       Enable debug mode.
-h, --help        Shows this usage instructions.

Example
$ shlibs.acorn.digest_per_image parameters ubuntu:22.04 /tmp/digests.env

EOF
}

shlibs.acorn.digest_per_image() {
    local OUTPUT_FILE=""
    local DEBUG=false
    local WITH_EXPORT=""
    local SHLIBS_LOGGING_LEVEL="${SHLIBS_LOGGING_LEVEL:-info}"

    VALID_ARGS=$(getopt -o eo:dh --long with-export,output:,debug,help -- "$@")
    if [[ $? -ne 0 ]]; then
        return 1
    fi

    eval set -- "$VALID_ARGS"
    while [ : ]; do
        case "$1" in
        -o | --output)
            OUTPUT_FILE="$2"
            shift 2
            ;;
        -e | --with-export)
            WITH_EXPORT="export "
            shift
            ;;
        -d | --debug)
            DEBUG=true
            SHLIBS_LOGGING_LEVEL="debug"
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            shlibs.log_error "Unknown parameter: '$1'"
            shlibs.acorn.extract_image_from_acorn.usage
            break
            ;;
        esac
    done

    if [ -z ${1} ]; then
        shlibs.log_error "SOURCE_GUN is unset"
        shlibs.acorn.extract_image_from_acorn.usage
        return 1
    fi
    local SOURCE_GUN="${1}"
    shift

    if [ ! -z ${OUTPUT_FILE} ]; then
        cat /dev/null >|"${OUTPUT_FILE}"
    fi

    if [ -z ${OUTPUT_FILE} ]; then
        shlibs.log_debug "OUTPUT_FILE is unset, don't exporting env"
    fi

    MANIFEST_DIGESTS=($(crane manifest ${SOURCE_GUN} | jq -r .manifests[].digest))
    shlibs.log_debug "The manifest containts the following MANIFEST_DIGESTS: ${MANIFEST_DIGESTS[@]}"

    for IMAGE_DIGEST in "${MANIFEST_DIGESTS[@]}"; do
        SOURCE_GUN_SUBIMAGE="${SOURCE_GUN}@${IMAGE_DIGEST}"
        IMAGE_NAME=$(crane config "${SOURCE_GUN_SUBIMAGE}" | jq -r '.config.Labels."org.label-schema.name"')
        shlibs.log_info "Found image named: '${IMAGE_NAME}' with DIGEST: ${SOURCE_GUN}@${IMAGE_DIGEST}"
        if [ ! -z ${OUTPUT_FILE} ]; then
            # ^^ means convertion to uppercase
            ENV_NAME=GUN_${IMAGE_NAME^^}
            # replace - with _
            ENV_NAME=${ENV_NAME//-/_}

            # # skip the acorn app image
            # if [ "${IMAGE_NAME}" != "null" ]; then
            echo "${WITH_EXPORT}${ENV_NAME}=${SOURCE_GUN}@${IMAGE_DIGEST}" >>"${OUTPUT_FILE}"
            # fi
        else
            shlibs.log_debug "OUTPUT_FILE is unset"
        fi
    done
}

shlibs.skaffold-artifacts-to-single-arch-manifest.usage() {
    cat <<EOF
shlibs.skaffold-artifacts-to-single-arch-manifest

Takes an artifacts.json created by 'skaffold build --file-output' and adds the OS and ARCH.
The resulting file can later be used as input for 'shlibs.build-multiarch-manifest-from-single-manifests' to create a multiarch image.

Parameters
-a, --arch                  The target arch of the image, e.g. TARGETARCH. Defaults to 'amd64'.
-o, --os                    The target os of the image, e.g. TARGETOS. Defaults to 'linux'.
-w, --manifests-dir         The directory where to write the manifests file to. Defaults to './manifest'.
-p, --manifests-prefix      The prefix used to name the manifests file. Defaults to 'manifest-'.
-f, --artifacts             The skaffold build output artifacts json. Defaults to 'artifacts-${OS}-${ARCH}.json'.

-d, --debug                 Enable debug mode.
-h, --help                  Shows this usage instructions.

EOF
}

shlibs.skaffold-artifacts-to-single-arch-manifest() {
    local ARCH="amd64"
    local OS="linux"
    local MANIFESTS_DIR="manifests"
    local MANIFEST_PREFIX="manifest-"
    local ARTIFACTS="artifacts-${OS}-${ARCH}.json"
    local DEBUG=false
    local SHLIBS_LOGGING_LEVEL="${SHLIBS_LOGGING_LEVEL:-info}"

    VALID_ARGS=$(getopt -o a:o:w:f:p:dh --long arch:,os:,manifests-dir:,artifacts:,manifest-prefix:,debug,help -- "$@")
    if [[ $? -ne 0 ]]; then
        return 1
    fi

    eval set -- "$VALID_ARGS"
    while [ : ]; do
        case "$1" in
        -a | --arch)
            ARCH="$2"
            shift 2
            ;;
        -o | --os)
            OS="$2"
            shift 2
            ;;
        -w | --manifests-dir)
            MANIFESTS_DIR="$2"
            shift 2
            ;;
        -f | --artifacts)
            ARTIFACTS="$2"
            shift 2
            ;;
        -p | --manifests-prefix)
            MANIFEST_PREFIX="$2"
            shift 2
            ;;
        -d | --debug)
            DEBUG=true
            SHLIBS_LOGGING_LEVEL="debug"
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            shlibs.log_error "Unknown parameter: '$1'"
            shlibs.skaffold-artifacts-to-single-arch-manifest.usage
            return 1
            ;;
        esac
    done

    shlibs.log_debug "MANIFEST_PREFIX: ${MANIFEST_PREFIX}"
    shlibs.log_debug "MANIFESTS_DIR: ${MANIFESTS_DIR}"
    shlibs.log_debug "ARCH: ${ARCH}"
    shlibs.log_debug "OS: ${OS}"
    shlibs.log_debug "ARTIFACTS: ${ARTIFACTS}"

    if ! shlibs.os.has_executables cat jq mv mktemp; then
        shlibs.log_error "dependencies not matched".
        return 1
    fi

    mkdir -p "${MANIFESTS_DIR}"
    TMPFILE=$(mktemp /tmp/${MANIFEST_PREFIX}-temp.XXXXXX)

    if [[ ! -f "${ARTIFACTS}" ]]; then
        shlibs.log_error "Unable to find artifacts file '${ARTIFACTS}'"
        return 1
    fi

    SUBIMAGE_COUNT=$(jq '.builds | length' "${ARTIFACTS}")
    for ((i = 0; i < ${SUBIMAGE_COUNT}; i++)); do
        export SUBIMAGE=$(jq -r --argjson index $i '.builds[$index].imageName | split("/")[-1]' "${ARTIFACTS}")
        export MANIFEST="${MANIFESTS_DIR}/${MANIFEST_PREFIX}${SUBIMAGE}-${OS}-${ARCH}.json"
        shlibs.log_debug "processing artifacts: ${ARTIFACTS}"
        shlibs.cat_debug "${ARTIFACTS}"
        shlibs.log_debug "creating manifest: ${MANIFEST}"

        IMAGE_NAME=$(jq --argjson index $i '.builds[$index].imageName' "${ARTIFACTS}")
        DIGEST=$(jq --argjson index $i '.builds[$index].tag | split("@")[-1]' "${ARTIFACTS}")
        # TODO digest korrekt abgreifen, was tun, wenn nicht im artifact enthalten?
        shlibs.log_debug "Found DIGEST: ${DIGEST}"
        TAG=$(jq --argjson index $i '.builds[$index].tag | split("@")[0] | split(":")[-1]' "${ARTIFACTS}")
        # shlibs.log_debug "Adding TAG: ${TAG}"
        IMAGE_GUN=$(jq --argjson index $i '.builds[$index].tag' "${ARTIFACTS}")
        jq -n --argjson image_name "${IMAGE_NAME}" --argjson image_gun "${IMAGE_GUN}" --argjson tag "${TAG}" --argjson digest "${DIGEST}" '.imageName = $image_name | .imageGun = $image_gun | .tag= $tag | .digest = $digest' >${MANIFEST}
        jq --arg os "${OS}" --arg arch "$ARCH" '. + {platforms: {os: $os, arch: $arch}}' "${MANIFEST}" >"${TMPFILE}"
        mv "${TMPFILE}" "${MANIFEST}"
        shlibs.cat_debug "${MANIFEST}"
    done
    shlibs.log_info "created ${MANIFEST}"
}

shlibs.build-multiarch-manifest-from-single-manifests.usage() {
    cat <<EOF
shlibs.build-multiarch-manifest-from-single-manifests

Takes an single arch manifest created by 'shlibs.skaffold-artifacts-to-single-arch-manifest' and
converts it into arch and os-specific multi-arch-manifests, that can be used by 'manifest-tool'
to create a multiarch image.

Parameters
-a, --archs                         The list of target archs as string formatted, space seperated list. Defaults to "amd64 arm64".
-o, --oss                           The list of target oss as string formatted, space seperated list: Defaults to 'linux'.
-s, --subimages                     The list of subimages for the multiarch manifests as string formatted, space seperated list: "nginx test123"
-t, --tags                          The tag list for the multiarch manifests as string formatted, space seperated list.
-w, --manifests-dir                 The directory where to the single image manifests are located. Defaults to './manifest'.
-m, --multiarch-manifests-prefix    The prefix used to name the multiarch manifests file. Defaults to 'multiarch-manifest-'.
-p, --manifests-prefix              The prefix used to name the manifests file. Defaults to 'manifest-'.

-d, --debug                         Enable debug mode.
-h, --help                          Shows this usage instructions.

EOF
}

shlibs.build-multiarch-manifest-from-single-manifests() {
    local ARCHS=()
    local OSS=()
    local SUBIMAGES=()
    local TAGS=()
    local MANIFESTS_DIR="manifests"
    local MULTIARCH_MANIFEST_PREFIX="multiarch-manifest-"
    local MANIFEST_PREFIX="manifest-"
    local DEBUG=false
    local SHLIBS_LOGGING_LEVEL="${SHLIBS_LOGGING_LEVEL:-info}"

    VALID_ARGS=$(getopt -o a:o:s:t:w:m:p:dh --long archs:,oss:,subimages:,tags:,manifests-dir:,multiarch-manifest-prefix:,manifest-prefix:,debug,help -- "$@")
    if [[ $? -ne 0 ]]; then
        return 1
    fi

    eval set -- "$VALID_ARGS"
    while [ : ]; do
        case "$1" in
        -a | --archs)
            IFS=' ' read -r -a TMP_ARRAY <<<"$2"
            ARCHS+=("${TMP_ARRAY[@]}")
            unset TMP_ARRAY
            shift 2
            ;;
        -o | --oss)
            IFS=' ' read -r -a TMP_ARRAY <<<"$2"
            OSS+=("${TMP_ARRAY[@]}")
            unset TMP_ARRAY
            shift 2
            ;;
        -s | --subimages)
            IFS=' ' read -r -a TMP_ARRAY <<<"$2"
            SUBIMAGES+=("${TMP_ARRAY[@]}")
            unset TMP_ARRAY
            shift 2
            ;;
        -t | --tags)
            IFS=' ' read -r -a TMP_ARRAY <<<"$2"
            TAGS+=("${TMP_ARRAY[@]}")
            unset TMP_ARRAY
            shift 2
            ;;
        -w | --manifests-dir)
            MANIFESTS_DIR="$2"
            shift 2
            ;;
        -m | --multiarch-manifests-prefix)
            MULTIARCH_MANIFEST_PREFIX="$2"
            shift 2
            ;;
        -p | --manifests-prefix)
            MANIFEST_PREFIX="$2"
            shift 2
            ;;
        -d | --debug)
            DEBUG=true
            SHLIBS_LOGGING_LEVEL="debug"
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            shlibs.log_error "Unknown parameter: '$1'"
            shlibs.skaffold-artifacts-to-single-arch-manifest.usage
            return 1
            ;;
        esac
    done

    # set defaults, if no values are passed
    if (("${#ARCHS[@]}" == 0)); then
        ARCHS+=("amd64" "arm64")
    fi
    if (("${#OSS[@]}" == 0)); then
        OSS+=("linux")
    fi

    shlibs.log_debug "OSS: ${OSS[@]}"
    shlibs.log_debug "ARCHS: ${ARCHS[@]}"
    shlibs.log_debug "SUBIMAGES: ${SUBIMAGES[@]}"
    shlibs.log_debug "TAGS: ${TAGS[@]}"
    shlibs.log_debug "MANIFESTS_DIR: ${MANIFESTS_DIR}"
    shlibs.log_debug "MANIFEST_PREFIX: ${MANIFEST_PREFIX}"
    shlibs.log_debug "MULTIARCH_MANIFEST_PREFIX: ${MULTIARCH_MANIFEST_PREFIX}"

    if ! shlibs.os.has_executables cat mktemp head ls jq yq; then
        shlibs.log_error "dependencies not matched".
        return 1
    fi

    for SUBIMAGE in "${SUBIMAGES[@]}"; do
        # a temp file is needed, cause jq can't update the file it reads from, due to shell limitations
        export tmpfile=$(mktemp /tmp/${MULTIARCH_MANIFEST_PREFIX}${subimage}temp.XXXXXX)
        export multiarch_json=$(mktemp /tmp/${MULTIARCH_MANIFEST_PREFIX}manifest${subimage}.json.XXXXXX)

        shlibs.log_debug "processing subimage: ${SUBIMAGE}"
        MULTIARCH_MANIFEST="${MANIFESTS_DIR}/${MULTIARCH_MANIFEST_PREFIX}${SUBIMAGE}.yaml"
        shlibs.log_info "creating: ${MULTIARCH_MANIFEST}"

        manifest="${MANIFESTS_DIR}/${MANIFEST_PREFIX}${SUBIMAGE}-${OSS[0]}-${ARCHS[0]}.json"
        shlibs.log_debug "using manifest: ${manifest}"

        IMAGE_NAME_MULTIARCH=$(jq '.imageName' "${manifest}")
        TAG=$(jq -r '.tag' "${manifest}")
        # remove _${OS}_${ARCH} part from tag
        TAG=$(echo ${TAG} | cut -d '_' -f1)
        TAGS+=($TAG)
        shlibs.log_debug "IMAGE_NAME_MULTIARCH: ${IMAGE_NAME_MULTIARCH}"

        GUN_MULTIARCH=$(jq '.imageGun | split("@")[0]' "${manifest}")
        jq -n --argjson image_name_multiarch "${IMAGE_NAME_MULTIARCH}" '.image = $image_name_multiarch | .tags = [] | .manifests = []' >"${multiarch_json}"
        shlibs.cat_debug "${multiarch_json}"

        for TAG in "${TAGS[@]}"; do
            jq --arg tag "${TAG}" '.tags |= (. += [$tag] | unique)' "${multiarch_json}" >"${tmpfile}"
            mv "${tmpfile}" "${multiarch_json}"
        done
        shlibs.cat_debug "${multiarch_json}"

        for OS in "${OSS[@]}"; do
            for ARCH in "${ARCHS[@]}"; do
                shlibs.log_debug "ARCH: ${ARCH}"
                shlibs.log_debug "OS: ${OS}"
                export manifest="${MANIFESTS_DIR}/${MANIFEST_PREFIX}${SUBIMAGE}-${OS}-${ARCH}.json"
                if [ -f ${manifest} ]; then
                    shlibs.log_debug "processing ${manifest}"
                    shlibs.cat_debug "${multiarch_json}"
                    jq --argjson manifest "$(<${manifest})" '.manifests += [$manifest]' "${multiarch_json}" >"${tmpfile}"
                    shlibs.cat_debug "${tmpfile}"
                    mv "${tmpfile}" "${multiarch_json}"

                    jq '.manifests |= map(. + {image: .imageGun})' "${multiarch_json}" >"${tmpfile}"
                    shlibs.cat_debug "${tmpfile}"
                    mv "${tmpfile}" "${multiarch_json}"
                else
                    shlibs.log_error "unable to find ${manifest}"
                fi
            done
        done

        # manifest tool wants yaml, so use yq to convert it
        yq -p json -o yaml "${multiarch_json}" >"${MULTIARCH_MANIFEST}"
        # ls ${MULTIARCH_MANIFEST}
        shlibs.cat_debug "${MULTIARCH_MANIFEST}"
    done
}

shlibs.skaffold.build-for-platform.usage() {
    cat <<EOF
shlibs.skaffold.build

This is wrapper around 'skaffold build' to simplify the building of a single arch image with artifact output.
It's useful to simplify the efficient building of multiarch images.

Parameters
-a, --arch          The target arch of the image, e.g. TARGETARCH. Defaults to 'amd64'.
-o, --os            The target os of the image, e.g. TARGETOS. Defaults to 'linux'.
-f, --artifacts     Output file to write. Defaults to 'artifacts-<OS>-<ARCH>.json
-p, --push          Adds --push to skaffold build to push images after sucessful build. Defaults to 'false'.
-A, --args          Add more args to skaffold. Can be used multiple times or as escaped, space seperated list, e.g. '--args "--debug --verbose"' or '--args "--debug" --args "--verbose"'

-d, --debug         Enable debug mode.
-h, --help          Shows this usage instructions.

EOF
}

shlibs.skaffold.build-for-platform() {
    local ARCH="amd64"
    local OS="linux"
    local ARGS=()
    local PUSH=false
    local PUSH_ARG=""
    local DEBUG=false
    local SHLIBS_LOGGING_LEVEL="${SHLIBS_LOGGING_LEVEL:-info}"

    VALID_ARGS=$(getopt -o a:o:f:pA:dh --long arch:,os:,artifacts:,push,args:,debug,help -- "$@")
    if [[ $? -ne 0 ]]; then
        return 1
    fi

    eval set -- "$VALID_ARGS"
    while [ : ]; do
        case "$1" in
        -a | --arch)
            ARCH="$2"
            shift 2
            ;;
        -o | --os)
            OS="$2"
            shift 2
            ;;
        -f | --artifacts)
            ARTIFACTS="$2"
            shift 2
            ;;
        -A | --args)
            IFS=' ' read -r -a TMP_ARRAY <<<"$2"
            ARGS+=("${TMP_ARRAY[@]}")
            unset TMP_ARRAY
            shift 2
            ;;
        -p | --push)
            PUSH=true
            shift
            ;;
        -d | --debug)
            DEBUG=true
            SHLIBS_LOGGING_LEVEL="debug"
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            shlibs.log_error "Unknown parameter: '$1'"
            shlibs.skaffold.build-for-platform.usage
            return 1
            ;;
        esac
    done

    if $PUSH; then
        PUSH_ARG="--push"
    fi

    local ARTIFACTS="artifacts-${OS}-${ARCH}.json"
    local PLATFORM_ARG="--platform ${OS}/${ARCH}"
    local OUTPUT_ARG="--file-output ${ARTIFACTS}"

    shlibs.log_debug "ARCH: ${ARCH}"
    shlibs.log_debug "OS: ${OS}"
    shlibs.log_debug "PUSH: ${PUSH_ARG}"
    shlibs.log_debug "ARTIFACTS: ${ARTIFACTS}"
    shlibs.log_debug "ARGS: ${ARGS[@]}"

    if ! shlibs.os.has_executables skaffold; then
        shlibs.log_error "dependencies not matched".
        return 1
    fi

    skaffold build ${PUSH_ARG} ${PLATFORM_ARG} ${OUTPUT_ARG} ${ARGS[@]}

    shlibs.log_info "artifacts written to: ${ARTIFACTS}"
    shlibs.cat_debug "${ARTIFACTS}"
}

shlibs.manifest-tool.build-multiarch-from-manifest.usage() {
    cat <<EOF
shlibs.manifest-tool.build-multiarch-from-manifest

This is a wrapper around [manifest-tool](https://github.com/estesp/manifest-tool) to loop over a given directory
containing multiarch manifests created with shlibs.build-multiarch-manifest-from-single-manifests.

You can create an image for a specific --manifest or for multiple manifests residing in --manifests-dir.

Parameters
-m, --manifest          The manifests the images should be created from. Can be empty.
-d, --manifests-dir     The directory the manifests are located in. Defaults to 'manifests'. If --manifest is passed, --manifests-dir will be ignored.
-p, --prefix            Prefix used for the multiarch-manifests. Defaults to 'multiarch-manifest-'
-u, --username          The username to login to the target registry.
-P, --password          The password to login to the target registry.
-p, --password-stdin    Pipe in password: 'echo "password" | shlibs.manifest-tool.build-multiarch-from-manifest'

-d, --debug             Enable debug mode.
-h, --help              Shows this usage instructions.

EOF
}

shlibs.manifest-tool.build-multiarch-from-manifest() {
    local USERNAME="gitlab-ci-token"
    local PASSWORD=""
    local PASSWORD_STDIN=false
    local MANIFEST=""
    local MANIFESTS_DIR="manifests"
    local MULTIARCH_MANIFEST_PREFIX="multiarch-manifest-"

    local DEBUG=false
    local SHLIBS_LOGGING_LEVEL="${SHLIBS_LOGGING_LEVEL:-info}"

    VALID_ARGS=$(getopt -o u:P:m:f:p:dh --long username:,password:,manifest:,manifests-dir:,prefix:,password-stdin,debug,help -- "$@")
    if [[ $? -ne 0 ]]; then
        return 1
    fi

    eval set -- "$VALID_ARGS"
    while [ : ]; do
        case "$1" in
        -m | --manifest)
            MANIFEST="$2"
            shift 2
            ;;
        -d | --manifests-dir)
            MANIFESTS_DIR="$2"
            shift 2
            ;;
        -p | --prefix)
            MULTIARCH_MANIFEST_PREFIX="$2"
            shift 2
            ;;
        -u | --username)
            USERNAME="$2"
            shift 2
            ;;
        -P | --password)
            PASSWORD="$2"
            shift 2
            ;;
        -p | --password-stdin)
            PASSWORD_STDIN=true
            shift
            ;;
        -d | --debug)
            DEBUG=true
            SHLIBS_LOGGING_LEVEL="debug"
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            shlibs.log_error "Unknown parameter: '$1'"
            shlibs.manifest-tool.build-multiarch-from-manifest.usage
            return 1
            ;;
        esac
    done

    local ARTIFACTS="artifacts-${OS}-${ARCH}.json"
    local PLATFORM_ARG="--platform ${OS}/${ARCH}"
    local OUTPUT_ARG="--file-output ${ARTIFACTS}"

    if ! shlibs.os.has_executables manifest-tool; then
        shlibs.log_error "dependencies not matched".
        return 1
    fi

    # if [ -z "${SUBIMAGE}" ]; then
    #     shlibs.log_error "No '--subimage' defined"
    #     return 2
    # fi

    if [ -z "${PASSWORD}" ]; then
        shlibs.log_error "No 'password' defined"
        read -s -p "PASSWORD: " -e PASSWORD
    fi

    if $PASSWORD_STDIN; then
        read -s -p "PASSWORD: " -e PASSWORD
    fi

    shlibs.log_debug "MULTIARCH_MANIFEST_PREFIX: ${MULTIARCH_MANIFEST_PREFIX}"
    shlibs.log_debug "MANIFESTS_DIR: ${MANIFESTS_DIR}"
    shlibs.log_debug "SUBIMAGE: ${SUBIMAGE}"
    shlibs.log_debug "PASSWORD: ${PASSWORD}"

    if [ "${MANIFEST}" ]; then
        shlibs.log_info "Creating image from ${MANIFEST}"
        shlibs.cat_debug "${MANIFEST}"
        manifest-tool --username ${USERNAME} --password ${PASSWORD} push from-spec ${MANIFEST}
        IMAGE=$(yq '.image' "${MANIFEST}")
        TAG=$(yq '.tags[0]' "${MANIFEST}")
        REGISTRY=$(echo "${IMAGE}" | cut -d "/" -f 1)
        IMAGE="${IMAGE}:${TAG}"
        shlibs.crane.login --registry "${REGISTRY}" --username "${USERNAME}" --password "${PASSWORD}"
        shlibs.log_info "Manifest for ${IMAGE}"
        crane manifest "${IMAGE}" | jq
        return 0
    fi

    if [ "${MANIFESTS_DIR}" ]; then
        manifests=$(ls ${MANIFESTS_DIR}/${MULTIARCH_MANIFEST_PREFIX}*.yaml)
        shlibs.log_info "Found the following manifests in ${MANIFESTS_DIR}: ${manifests}"
        for manifest in ${manifests}; do
            shlibs.cat_debug "${manifest}"
            manifest-tool --username ${USERNAME} --password ${PASSWORD} push from-spec ${manifest}
            IMAGE=$(yq '.image' "${MANIFEST}")
            TAG=$(yq '.tags[0]' "${MANIFEST}")
            REGISTRY=$(echo "${IMAGE}" | cut -d "/" -f 1)
            shlibs.crane.login --registry "${REGISTRY}" --username "${USERNAME}" --password "${PASSWORD}"
            IMAGE="${IMAGE}:${TAG}"
            shlibs.log_info "Manifest for ${IMAGE}"
            crane manifest "${IMAGE}" | jq
        done
        return 0
    fi

}
