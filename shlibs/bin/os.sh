#!/usr/bin/env bash

shlibs.os.distribution_id() {
    cat /etc/*release | grep "^ID=" | cut -f 2 -d "="
}

shlibs.os.type() {
    echo "${OSTYPE:-unkown}"
}

shlibs.os.type.base() {
    shlibs.os.type | cut -f 1 -d "-"
}

shlibs.os.has_executable() {
    local executable="$1"
    res=$(command -v "${executable}")
    # shlibs.log_debug "checking for '${executable}'"
    if [ "${res}" ] &>/dev/null; then
        # shlibs.log_debug "Found executable: '${executable}'"
        return 0
    else
        shlibs.log_error "Unable to find executable: '${executable}'"
        return 1
    fi
}

shlibs.os.has_executables() {
    #### usage
    # if ! shlibs.os.has_executables cat jq mv mktemp; then
    # shlibs.log_error "dependencies not matched".
    #    return 1
    # fi
    ###
    local executables=($@)
    for executable in "${executables[@]}"; do
        if ! shlibs.os.has_executable "${executable}"; then
            return 1
        fi
    done
    return 0
}

shlibs.os.test() {
    shlibs.os.has_executables yq jq curl test curl123 curl
}
