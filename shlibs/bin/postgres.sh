shlibs.get_md5_passwd() {
    local user="$1"
    local password="$2"
    echo "md5$(echo -n "${password}${user}" | md5sum | cut -f 1 -d ' ')"
}
