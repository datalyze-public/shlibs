#!/usr/bin/env bash

shlibs.sleep_infinity() {
    sleep infinity
}

shlibs.wait_indefinitely() {
    while true; do
        tail -f /dev/null &
        wait "${!}"
    done
}

shlibs.wait_for_postgres() {
    local host="${1:-127.0.0.1}"
    local port="${2:-5432}"
    local sleeptime="${3:-5}"
    local user="${4:-root}"

    shlibs.log_info "Checking for postgres..."
    until pg_isready -U $user -q -h ${host} -p ${port}; do
        shlibs.log_err "Postgres is unavailable - retrying ${host}:${port} in a ${sleeptime} seconds"
        sleep ${sleeptime}
    done
    shlibs.log_info "Postgres is available"
}
