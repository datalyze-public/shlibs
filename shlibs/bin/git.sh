shlibs.git.remove_git_keywords() {
    local name="$1"

    local keywords=(deployed-feature- feat-)

    for str_to_replace in "${keywords[@]}"; do
        name="${name/${str_to_replace}/}"
    done
    echo "${name}"
}

shlibs.git._shorten_name_usage() {
    cat <<EOF
shlibs.git.shorten_name parameters [<parameters>]  <name>

Takes a name, e.g. git branch name and shortens it to a specified length. If the name is to long, a md5 hash of the name is appended.
This is useful if you want to use the branch name as tag or dns prefix.

Parameters
-m, --max-chars   Maximum length of the name string.
-h, --help        Shows this usage instructions.

Example
$ shlibs.git.shorten_name feat-the-biggest-feature-ever-made
$ the-biggest-featurf9c28086

EOF
}

shlibs.git.shorten_name() {
    local max_chars="27"
    local hash_length="8"

    VALID_ARGS=$(getopt -o m:h --long max-chars:,help -- "$@")
    if [[ $? -ne 0 ]]; then
        return 1
    fi

    eval set -- "$VALID_ARGS"
    while [ : ]; do
        case "$1" in
        -m | --max-chars)
            if [[ $2 -ge ${hash_length} ]]; then
                max_chars="$2"
            else
                shlibs.log_error "--max-chars needs to to be bigger than ${hash_length}"
                return 1
            fi
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            shlibs.log_error "Unknown parameter: '$1'"
            shlibs.git._shorten_name_usage
            break
            ;;
        esac
    done

    local name="$1"
    local max_chars_with_sha=$(($max_chars - $hash_length))

    local name_hash=$(echo ${name} | md5sum)
    local name_hash_short="${name_hash:0:${hash_length}}"
    local name_without_git_keywords=$(shlibs.git.remove_git_keywords "${name}")
    local shortened_name="${name_without_git_keywords}"
    shlibs.log_debug "without git keywords: ${name_without_git_keywords}"

    if [ "${#name_without_git_keywords}" -ge "${max_chars}" ]; then
        # append md5sum to name if its too long
        # remove one more char from name to add a before the hash
        last_char_index=$(($max_chars_with_sha - 1))
        shortened_name="${name_without_git_keywords:0:${last_char_index}}-${name_hash_short}"
        # in some cases the last char might be a -, so remove double minuses
        shortened_name="${shortened_name//--/-}"
        shlibs.log_info "name shortened to ${shortened_name}"
    else
        shlibs.log_debug "no need to shorten name"
    fi

    echo "${shortened_name}"
}
