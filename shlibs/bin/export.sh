#!/usr/bin/env bash

shlibs.export_functions_to_subshell() {
  shlibs_dir=$(dirname "${BASH_SOURCE[0]}")
  shlibs_bin_dir="${shlibs_dir}/bin"

  # export namespaced shlibs functions to be used in child shells
  for F in $(declare -F | grep -e shlibs | cut -f 3 -d " "); do
    shlibs.log_debug "found shlibs function for export: $F"
    export -f "$F"
  done

  if [ "${SHLIBS_DEBUG}" = true ]; then
    declare -F | grep "shlibs"
  fi
}
