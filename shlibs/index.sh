#!/usr/bin/env bash

SHLIBS_DEBUG="${SHLIBS_DEBUG:-false}"
SHLIBS_EXPORT_TO_SUBSHELL="${SHLIBS_EXPORT_TO_SUBSHELL:-false}"

shlibs_dir=$(dirname "${BASH_SOURCE[0]}")
shlibs_bin_dir="${shlibs_dir}/bin"

shlibs.index.usage() {
    cat <<EOF
shlibs

Parameters
-d, --debug                             Enable debugging, sets SHLIBS_DEBUG=true.
-r, --reload                            Reload the shlibs functions. Useful on development.
-e, --export, --export-for-subshells    Exports all shlibs functions (e.g. export -f shlibs.some-function).
                                        Enables usage of shlibs inside subshells, e.g. in CI or task runner tools.
-h, --help                              Shows this usage instructions.

Examples
$ source <(/usr/local/bn/shlibs/index.sh --reload --export)
$ shlibs.log_info "Hello World!"
$ bash -c "shlibs.log_info "Hello World from this subshell!!!"

EOF
}

case "${SHELL}" in
*"ash"* | *"bash"* | *"sh"*)

    VALID_ARGS=$(getopt -o dehr --long debug,export,help,reload -- "$@")
    if [[ $? -ne 0 ]]; then
        return 1
    fi

    eval set -- "$VALID_ARGS"
    while true; do
        case "$1" in
        -d | --debug)
            SHLIBS_DEBUG=true
            shift
            ;;
        -e | --export | --export-for-subshells)
            SHLIBS_EXPORT_TO_SUBSHELL=true
            shift
            ;;
        -r | --reload)
            echo >&2 "Reloading shlibs functions"
            source "${shlibs_dir}/unset.sh"
            shift
            ;;
        -h | --help)
            shlibs.index.usage
            break
            return
            ;;
        --)
            shift
            break
            ;;
        esac
    done

    for file in $(find -L "${shlibs_bin_dir}" -maxdepth 1 ! -path "${shlibs_dir}/index.sh" ! -path "${shlibs_dir}/unset.sh" -name "*.sh" | sort); do
        source "${file}"
    done

    if [ "${SHLIBS_EXPORT_TO_SUBSHELL}" = true ]; then
        shlibs.export_functions_to_subshell
    fi

    ;;
*)
    echo >&2 "shlibs.import-error: Unsupported shell (${SHELL})"
    return 2
    ;;
esac
