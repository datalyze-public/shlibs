#!/usr/bin/env bash

shlibs_dir=$(dirname "${BASH_SOURCE[0]}")
shlibs_bin_dir="${shlibs_dir}/bin"

_log_debug() {
  if [ "${SHLIBS_DEBUG:-false}" = true ]; then
    echo >&2 "$@"
  fi
}

# unset namespaced shlibs functions
functions=($(declare -F | grep -e shlibs | cut -f 3 -d " "))
# echo "${functions[*]}"
for func in "${functions[@]}"; do
  _log_debug "found namespaced shlibs function: $func"
  unset -f "$func"
done

# # unset all other shlibs functions
# for file in $(find -L "${shlibs_bin_dir}" -maxdepth 1 ! -path "${shlibs_dir}/index.sh" ! -path "${shlibs_dir}/unset.sh" -name "*.sh" | sort); do
#   functions=$(grep -E '^[[:space:]]*([[:alnum:]_]+[[:space:]]*\(\)|function[[:space:]]+[[:alnum:]_]+)' "$file" | cut -f 1 -d "(")
#   for func in "${functions[@]}"; do
#     _log_debug "found non namespaced shlibs function: $func"
#     unset -f "$func"
#   done
# done

if [ "${SHLIBS_DEBUG}" = true ]; then
  declare -F | grep "shlibs"
  echo >&2 "unloaded shlibs"
fi