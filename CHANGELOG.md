# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.2.0](https://gitlab.com/datalyze-public/shlibs/compare/v1.1.0...v1.2.0) (2023-09-15)


### Features

* add check for unsupported shell while sourcing index.sh ([2a47baf](https://gitlab.com/datalyze-public/shlibs/commit/2a47baf92f4e326169d81932cac873c20b96c6e6))
* **ci:** add standard-version ([247eaaa](https://gitlab.com/datalyze-public/shlibs/commit/247eaaa83d31c266a191e7cf452d2115883a1605))
* login helpers for tools: acorn, crane, buildah, docker, podman (shlibs.<tool>.login) ([1bdadd8](https://gitlab.com/datalyze-public/shlibs/commit/1bdadd8943ebc5fe8e5db003b13687b40c78f3ec))


### Bug Fixes

* add shlibs to ci image ([085df4a](https://gitlab.com/datalyze-public/shlibs/commit/085df4a6f079081e13007c29771d57ccf57c8931))
* **ci:** fix npm version ([28247da](https://gitlab.com/datalyze-public/shlibs/commit/28247daad7a2ca3d7ce60150b53cb37425fbd8f5))
* fix alpine version pinning syntax ([923519c](https://gitlab.com/datalyze-public/shlibs/commit/923519cf625f412b4d63176a2e20a07a85bdabd2))
* fix shlibs gun in ci image ([52335a6](https://gitlab.com/datalyze-public/shlibs/commit/52335a6557c84f9844f67d8a184e88fbe2a296e6))
* fix unset.sh to really unload all shlibs functions ([662faa4](https://gitlab.com/datalyze-public/shlibs/commit/662faa4880cb9741f1105e26dd3cbece9085880d))

## [1.1.0](https://gitlab.com/datalyze-public/shlibs/compare/v1.0.0...v1.1.0) (2023-09-12)


### Features

* include version into image tag: {VERSION}-{SHORT-SHA}-dirty and {VERSION}-{BRANCH} ([da6bbfa](https://gitlab.com/datalyze-public/shlibs/commit/da6bbfaf947ed653514a7cd7eead84511d52194b))


### Bug Fixes

* call git-mkver directly to fix ci errors ([dc15bd7](https://gitlab.com/datalyze-public/shlibs/commit/dc15bd763125e7fadc3fa39b9947221f9a2c505a))

## 1.0.0 (2023-09-12)


### ⚠ BREAKING CHANGES

* restructure shlibs directory; move scripts to ./bin subdir

### Features

* add script to compile shlibs to gitlab-ci consumable yaml ([630502e](https://gitlab.com/datalyze-public/shlibs/commit/630502eba7d3384c878ab4f1cbba87857d0072dc))
* add shlibs.arch to get name of the machines architecture, e.g. arm64, amd64 ([605bab6](https://gitlab.com/datalyze-public/shlibs/commit/605bab6a6d4d76288d788abbc01898d51b8a635e))
* add yamllint before copying compiled yaml result to ./yaml dir ([d76874e](https://gitlab.com/datalyze-public/shlibs/commit/d76874efd449372936c76a5d84666ee5b030eac5))


### Bug Fixes

* **ci:** install bash by default ([0ea0d96](https://gitlab.com/datalyze-public/shlibs/commit/0ea0d96d1b5bf291bdbc60f1d27563574b1e051d))
* **ci:** install git by default ([b10cb2d](https://gitlab.com/datalyze-public/shlibs/commit/b10cb2d6ce51ca1a686614c2738df1f24d4e64af))


* restructure shlibs directory; move scripts to ./bin subdir ([81cf455](https://gitlab.com/datalyze-public/shlibs/commit/81cf455687aaba0d2ade6486a707a1e43e1033e7))
